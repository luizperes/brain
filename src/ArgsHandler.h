/* This is the source code of Brain Programming Language.
 * It is licensed under GNU GPL v. 3 or later.
 * You should have received a copy of the license in this archive (see LICENSE).
 *
 * Copyright Luiz Peres, 2016.
 */

#ifndef ARGS_HANDLER_H
#define ARGS_HANDLER_H

#include <string>

class ArgsHandler 
{
  protected:
    std::string _stringFile;
    bool _isEmitingLLVM;
    bool _isEmitingExpr;
    bool _isOptimizing;
    void handle(int argc, char *argv[]);
  public:
    ArgsHandler(int argc, char *argv[]) : _isEmitingLLVM(false), _isEmitingExpr(false), _isOptimizing(true) { handle(argc, argv); }
    std::string getStringFile();
    bool isEmitLLVMActive();
    bool isEmitExprActive();
    bool isOptimizing();
};

#endif
